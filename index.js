const { createApp } = Vue
$V_MINI_NAV

createApp({
    data() {
        return {
            ...$V_MINI_NAV,
        }
    }
}).mount('#app')