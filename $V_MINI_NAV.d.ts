/**
 * __/mini_nav
 * mini导航
 * https://web-vars.top/var/index/__/mini_nav
 */
declare const $V_MINI_NAV: {
    /** 描述 */
    README: {
        /** 内容 */
        content: string;
        /** 显示内容 */
        showContent: string;
    };
    /** 数组 */
    list: {
        /** 字符串 */
        title: string;
        /** 数组 */
        list: {
            /** 字符串 */
            href: string;
            /** 字符串 */
            name: string;
            /** 字符串 */
            icon: string;
        }[];
    }[];
    /** 友情链接 */
    linkExchange: {
        /** 字符串 */
        href: string;
        /** 字符串 */
        name: string;
        /** 字符串 */
        icon: string;
    }[];
};
/**
 * 后执行脚本
*/
/**{{thenExecute}}*/
